package main

import (
	"log"
	"net/http"
	"os"

	"gitlab.com/shuwenhe/shuwen-storage/storage-many/heartbeat"
	"gitlab.com/shuwenhe/shuwen-storage/storage-many/locate"
	"gitlab.com/shuwenhe/shuwen-storage/storage-many/objects"
)

func main() {
	go heartbeat.StartHeartbeat()
	go locate.StartLocate()
	http.HandleFunc("/objects/", objects.Handler)
	log.Fatal(http.ListenAndServe(os.Getenv("LISTEN_ADDRESS"), nil))
}
