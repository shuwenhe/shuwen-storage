package locate

import (
	"os"
	"strconv"
)

// 用于实际定位对象的Locate
func Locate(name string) bool {
	_, err := os.Stat(name)    // 访问磁盘上对应的文件名
	return !os.IsNotExist(err) // 判断文件名是否存在=>如果存在=>返回true
}

// 用于监听定位消息的StartLocate
func StartLocate() {
	q := rabbitmq.New(os.Getenv("RABBITMQ_SERVER")) // 创建rabbitMQ结构体
	defer q.Close()
	q.Bind("dataServers") // 调用bind方法绑定dataServers exchange
	c := q.Consume()      // 返回一个channel
	for msg := range c {  // 遍历channel接收消息
		// strconv.Uniquote函数作用=>将输入的字符串前后的双引号去除并作为结果返回
		// 消息的正文内容是接口服务发送过来的需要做定位的对象名字
		object, err := strconv.Unquote(string(msg.Body))
		if err != nil {
			panic(err)
		}
		if Locate(os.Getenv("STORAGE_ROOT") + "/objects/" + object) {
			q.Send(msg.ReplyTo, os.Getenv("LISTEN_ADDRESS"))
		}
	}
}
