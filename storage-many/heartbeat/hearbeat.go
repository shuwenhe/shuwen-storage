package heartbeat

import (
	"os"
	"time"
)

func StartHeartbeat() {
	// 创建rabbitmq.RabbitMQ结构体
	q := rabbitmq.New(os.Getenv("RABBITMQ_SERVER"))
	defer q.Close()
	// 无限循环
	for {
		// 把本节点的监听地址发送出去
		q.Publish("apiServers", os.Getenv("LISTEN_ADDRESS"))
		time.Sleep(5 * time.Second) // 每5秒向apiServers exchange发送一个消息
	}
}
