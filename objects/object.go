package objects

import (
	"io"
	"log"
	"net/http"
	"os"
	"strings"
)

func Handler(w http.ResponseWriter, r *http.Request) {
	m := r.Method // r：当前处理的http的请求 Method成员变量记录http请求的方法
	if m == http.MethodPost {
		put(w, r)
		return
	}
	if m == http.MethodGet {
		get(w, r)
		return
	}
	// Write writes the data to the connection as part of an HTTP reply.
	// w.Write() // 写http响应的正文
	w.WriteHeader(http.StatusMethodNotAllowed) // 写HTTP响应的错误代码
}

func put(w http.ResponseWriter, r *http.Request) {
	// Split用分隔符将一个字符串分割成多个字符串，2个参数：第一个是需要分隔的字符串；第二个是分隔符
	// 分隔后的结果以字符串的切片形式返回、
	// os.Create在本地文件系统的根目录的object子目录下创建一个同名的文件file
	// 根目录由系统环境变量STORAGE_ROOT定义
	file, err := os.Create(os.Getenv("STORAGE_ROOT") + "/objects/" + strings.Split(r.URL.EscapedPath(), "/")[2])
	defer file.Close()
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError) // 如果创建失败则返回http错误码500
		return
	}
	// 如果创建成功则将r.Body用io.Copy写入文件file
	io.Copy(file, r.Body)
}

// get负责http的GET请求
// 从本地硬盘读取内容并将其作为HTTP的响应输出
func get(w http.ResponseWriter, r *http.Request) {
	file, err := os.Open(os.Getenv("STORAGE_ROOT") + "/objects/" + strings.Split(r.URL.EscapedPath(), "/")[2])
	defer file.Close()
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusNotFound) // 打开失败：返回HTTP错误码404
		return
	}
	// file作为读取内容的io.Reader,w作为写入的io.Writer
	io.Copy(w, file) // 打开成功：用io.Copy将file的内容写入w
}
