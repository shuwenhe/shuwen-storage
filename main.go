package main

import (
	"log"
	"net/http"
	"os"

	"gitlab.com/shuwenhe/shuwen-storage/objects"
)

func main() {
	http.HandleFunc("/objects/", objects.Handler)
	log.Fatal(http.ListenAndServe(os.Getenv("LISTEN_ADDRESS"), nil)) // 系统环境变量
}
